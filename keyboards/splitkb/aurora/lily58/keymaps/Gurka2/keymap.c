/* Copyright 2022 splitkb.com <support@splitkb.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include QMK_KEYBOARD_H
#include "keymap_swedish.h"

#define CAPSESC MT(MOD_LCTL, KC_ESCAPE)

enum layers {
    BASE = 0,
    SYMBOLS = 1,
    GAMING = 2,
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
    [BASE] = LAYOUT(
// ┌─────────┬─────────┬─────────┬─────────┬─────────┬─────────┐                         ┌─────────┬─────────┬─────────┬─────────┬─────────┬─────────┐
     SE_SECT,   SE_1,     SE_2,     SE_3,     SE_4,     SE_5,                               SE_6,     SE_7,     SE_8,     SE_9,     SE_0,     KC_NO,
// ├─────────┼─────────┼─────────┼─────────┼─────────┼─────────┤                         ├─────────┼─────────┼─────────┼─────────┼─────────┼─────────┤
     KC_TAB,    SE_Q,     SE_W,     SE_E,      SE_R,     SE_T,                              SE_Y,     SE_U,     SE_I,     SE_O,     SE_P,    SE_ARNG,
// ├─────────┼─────────┼─────────┼─────────┼─────────┼─────────┤                         ├─────────┼─────────┼─────────┼─────────┼─────────┼─────────┤
     KC_LSFT,   SE_A,     SE_S,     SE_D,     SE_F,     SE_G,                               SE_H,     SE_J,     SE_K,     SE_L,    SE_ODIA,  SE_ADIA,
// ├─────────┼─────────┼─────────┼─────────┼─────────┼─────────┼─────────┐     ┌─────────┼─────────┼─────────┼─────────┼─────────┼─────────┼─────────┤
     CAPSESC,   SE_Z,     SE_X,     SE_C,     SE_V,     SE_B,    KC_NO,           KC_NO,    SE_N,     SE_M,    SE_COMM,  SE_DOT,   SE_MINS,  KC_RSFT,
// └─────────┴─────────┴─────────┴─────────┴─────────┴─────────┴─────────┘     └─────────┴─────────┴─────────┴─────────┴─────────┴─────────┴─────────┘
                         KC_LALT,  KC_LGUI,  KC_SPC,   KC_BSPC,                             KC_ENT,   MO(1),    TO(2),    KC_NO
//                     └─────────┴─────────┴─────────┴─────────┘                         └─────────┴─────────┴─────────┴─────────┘

    ),
    [SYMBOLS] = LAYOUT(
// ┌─────────┬─────────┬─────────┬─────────┬─────────┬─────────┐                         ┌─────────┬─────────┬─────────┬─────────┬─────────┬─────────┐
     KC_F1,    KC_F2,    KC_F3,    KC_F4,    KC_F5,    KC_F6,                               KC_F7,    KC_F8,    KC_F9,   KC_F10,   KC_F11,   KC_F12,
// ├─────────┼─────────┼─────────┼─────────┼─────────┼─────────┤                         ├─────────┼─────────┼─────────┼─────────┼─────────┼─────────┤
     _______,  SE_EXLM,  SE_CIRC,  SE_LCBR,  SE_RCBR,  SE_PIPE,                            SE_BSLS,  SE_SLSH,  SE_ASTR,  SE_QUES,  SE_PLUS,  SE_EQL,
// ├─────────┼─────────┼─────────┼─────────┼─────────┼─────────┤                         ├─────────┼─────────┼─────────┼─────────┼─────────┼─────────┤
     _______,  SE_AT,    SE_DLR,   SE_LPRN,  SE_RPRN,  SE_GRV ,                            KC_LEFT,  KC_DOWN,   KC_UP,   KC_RGHT,  SE_DQUO,  SE_QUOT,
// ├─────────┼─────────┼─────────┼─────────┼─────────┼─────────┼─────────┐     ┌─────────┼─────────┼─────────┼─────────┼─────────┼─────────┼─────────┤
     _______,  SE_PERC,  SE_HASH,  SE_LBRC,  SE_RBRC,  SE_TILD,  _______,        _______,  _______,  SE_AMPR,  SE_LABK,  SE_RABK,  SE_MINS,  SE_UNDS,
// └─────────┴─────────┴─────────┴─────────┴─────────┴─────────┴─────────┘     └─────────┴─────────┴─────────┴─────────┴─────────┴─────────┴─────────┘
//                     ┌─────────┬─────────┬─────────┬─────────┐                         ┌─────────┬─────────┬─────────┬─────────┐
                         _______,  SE_COLN,  SE_SCLN,  _______,                            _______,  _______,  _______,  _______
//                     └─────────┴─────────┴─────────┴─────────┘                         └─────────┴─────────┴─────────┴─────────┘
    ),
    [GAMING] = LAYOUT(
// ┌─────────┬─────────┬─────────┬─────────┬─────────┬─────────┐                         ┌─────────┬─────────┬─────────┬─────────┬─────────┬─────────┐
     KC_ESC,    SE_1,     SE_2,     SE_3,     SE_4,     SE_5,                               SE_6,     SE_7,     SE_8,     SE_9,     SE_0,    SE_PLUS,
// ├─────────┼─────────┼─────────┼─────────┼─────────┼─────────┤                         ├─────────┼─────────┼─────────┼─────────┼─────────┼─────────┤
     KC_TAB,    SE_Q,     SE_W,     SE_E,     SE_R,     SE_T,                               SE_Y,     SE_U,     SE_I,     SE_O,     SE_P,    SE_ARNG,
// ├─────────┼─────────┼─────────┼─────────┼─────────┼─────────┤                         ├─────────┼─────────┼─────────┼─────────┼─────────┼─────────┤
     KC_LSFT,   SE_A,     SE_S,     SE_D,     SE_F,     SE_G,                               SE_H,     SE_J,     SE_K,     SE_L,    SE_ODIA,  SE_ADIA,
// ├─────────┼─────────┼─────────┼─────────┼─────────┼─────────┼─────────┐     ┌─────────┼─────────┼─────────┼─────────┼─────────┼─────────┼─────────┤
    MOD_LCTL,   SE_Z,     SE_X,     SE_C,     SE_V,     SE_B,    KC_NO,           KC_NO,    SE_N,     SE_M,    SE_COMM,  SE_DOT,   SE_MINS,  KC_RSFT,
// └─────────┴─────────┴─────────┴─────────┴─────────┴─────────┴─────────┘     └─────────┴─────────┴─────────┴─────────┴─────────┴─────────┴─────────┘
                         KC_LALT,  KC_LGUI,  KC_SPC,   KC_BSPC,                             KC_ENT,   MO(1),    TO(0),    KC_NO
//                     └─────────┴─────────┴─────────┴─────────┘                         └─────────┴─────────┴─────────┴─────────┘

    )
};
